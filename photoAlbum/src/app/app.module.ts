import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { CreateAlbumComponent } from './components/create-album/create-album.component';
import { AlbumsSelectionComponent } from './components/albums-selection/albums-selection.component';
import { ImageCardComponent } from './components/image-card/image-card.component';
import { PhotoAlbumPageComponent } from './pages/photo-album-page/photo-album-page.component';

@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    CreateAlbumComponent,
    AlbumsSelectionComponent,
    ImageCardComponent,
    PhotoAlbumPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
