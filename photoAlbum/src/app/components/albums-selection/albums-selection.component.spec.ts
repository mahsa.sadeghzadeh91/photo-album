import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlbumsSelectionComponent } from './albums-selection.component';

describe('AlbumsSelectionComponent', () => {
  let component: AlbumsSelectionComponent;
  let fixture: ComponentFixture<AlbumsSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlbumsSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumsSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
