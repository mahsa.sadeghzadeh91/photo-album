import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotoAlbumPageComponent } from './photo-album-page.component';

describe('PhotoAlbumPageComponent', () => {
  let component: PhotoAlbumPageComponent;
  let fixture: ComponentFixture<PhotoAlbumPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhotoAlbumPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotoAlbumPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
